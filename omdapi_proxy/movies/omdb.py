from django.conf import settings

import requests
from movies.serializers import OMDBMovieSerializer


class APIError(Exception):
    pass


def fetch_movie_by_title(title):
    """
    Fetches movie data that matches given title.

    returns dict object with movie data
    raises APIError when the movie couldn't be found
    """
    response = requests.get('http://www.omdbapi.com/', {
        't': title,
        'apikey': settings.OMDB_API_KEY
    })
    if response.status_code != 200:
        raise APIError(response.content)
    data = response.json()
    error = data.get('Error', None)
    if error is not None:
        raise APIError(error)
    serializer = OMDBMovieSerializer(data=data)
    if not serializer.is_valid():
        raise APIError(serializer.errors)
    return serializer.save()
