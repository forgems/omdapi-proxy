# Generated by Django 2.1 on 2018-08-22 16:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0005_auto_20180822_0635'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movie',
            name='ratings',
        ),
        migrations.AlterField(
            model_name='movie',
            name='actors',
            field=models.ManyToManyField(related_name='played_in', to='movies.Person'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='awards',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='box_office',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='director',
            field=models.ManyToManyField(related_name='directed', to='movies.Person'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='genre',
            field=models.ManyToManyField(to='movies.Genre'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='imdb_id',
            field=models.CharField(max_length=64, unique=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='imdb_rating',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='imdb_votes',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='metascore',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='plot',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='poster',
            field=models.URLField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='production',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='rated',
            field=models.CharField(max_length=256),
        ),
        migrations.AlterField(
            model_name='movie',
            name='released',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='runtime',
            field=models.DurationField(),
        ),
        migrations.AlterField(
            model_name='movie',
            name='type',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='movie',
            name='website',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='movie',
            name='writers',
            field=models.ManyToManyField(related_name='wrote', to='movies.Person'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='year',
            field=models.PositiveIntegerField(),
        ),
    ]
