import datetime as dt
import json
import random

import factory
import responses
from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from movies import models
from movies.serializers import OMDBMovieSerializer

# Create your tests here.

EXAMPLE_DATA = """{
    "Title": "Guardians of the Galaxy Vol. 2",
    "Year": "2017",
    "Rated": "PG-13",
    "Released": "05 May 2017",
    "Runtime": "136 min",
    "Genre": "Action, Adventure, Sci-Fi",
    "Director": "James Gunn",
    "Writer": "James Gunn, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Steve Englehart (Star-Lord created by), Steve Gan (Star-Lord created by), Jim Starlin (Gamora and Drax created by), Stan Lee (Groot created by), Larry Lieber (Groot created by), Jack Kirby (Groot created by), Bill Mantlo (Rocket Raccoon created by), Keith Giffen (Rocket Raccoon created by), Steve Gerber (Howard the Duck created by), Val Mayerik (Howard the Duck created by)",
    "Actors": "Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel",
    "Plot": "The Guardians must fight to keep their newfound family together as they unravel the mystery of Peter Quill's true parentage.",
    "Language": "English",
    "Country": "USA",
    "Awards": "Nominated for 1 Oscar. Another 12 wins & 42 nominations.",
    "Poster": "https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_SX300.jpg",
    "Ratings": [
        {
            "Source": "Internet Movie Database",
            "Value": "7.7/10"
        },
        {
            "Source": "Rotten Tomatoes",
            "Value": "83%"
        },
        {
            "Source": "Metacritic",
            "Value": "67/100"
        }
    ],
    "Metascore": "67",
    "imdbRating": "7.7",
    "imdbVotes": "397,820",
    "imdbID": "tt3896198",
    "Type": "movie",
    "DVD": "22 Aug 2017",
    "BoxOffice": "$389,804,217",
    "Production": "Walt Disney Pictures",
    "Website": "https://marvel.com/guardians",
    "Response": "True"
}"""  # noqa

EXAMPLE_DATA = json.loads(EXAMPLE_DATA)


class MovieFactory(factory.Factory):
    year = 2000
    title = factory.Faker('company')
    released = factory.LazyAttribute(lambda o: dt.date.today())
    runtime = factory.LazyAttribute(
        lambda o: dt.timedelta(minutes=random.randrange(60, 300))
    )
    plot = 'Lorem ipsum'
    imdb_id = factory.Sequence(lambda n: '%08d' % n)
    imdb_votes = 100
    production = 'test'
    imdb_rating = 9.9
    poster = 'http://example.com/'
    rated = 'R'
    metascore = 100
    type = 'movie'
    box_office = 666
    website = 'N/A'
    awards = ''

    class Meta:
        model = models.Movie


class OMDBMovieSerializerTestCae(TestCase):
    def test_from_valid_omdbapi_response(self):
        # When
        serializer = OMDBMovieSerializer(data=EXAMPLE_DATA)

        # Then
        self.assertTrue(serializer.is_valid())

    def test_create_instance_from_omdbapi_response(self):
        # Given
        serializer = OMDBMovieSerializer(data=EXAMPLE_DATA)

        # When
        serializer.is_valid()
        movie = serializer.save()

        # Then
        self.assertIsNotNone(movie.pk)
        self.assertEqual(movie.title, EXAMPLE_DATA['Title'])
        self.assertEqual(movie.rated, EXAMPLE_DATA['Rated'])
        self.assertEqual(movie.website, EXAMPLE_DATA['Website'])
        self.assertEqual(movie.type, EXAMPLE_DATA['Type'])

    def test_create_actors_from_omdapi_response(self):
        # Given
        serializer = OMDBMovieSerializer(data=EXAMPLE_DATA)
        actors = [name.strip() for name in EXAMPLE_DATA['Actors'].split(', ')]

        # When
        serializer.is_valid()
        movie = serializer.save()

        # Then
        self.assertEqual(
            set(actor.name for actor in movie.actors.all()),
            set(actors)
        )

    def test_create_director_from_omdapi_response(self):
        # Given
        serializer = OMDBMovieSerializer(data=EXAMPLE_DATA)
        director = [
            name.strip() for name in EXAMPLE_DATA['Director'].split(', ')
        ]

        # When
        serializer.is_valid()
        movie = serializer.save()

        # Then
        self.assertEqual(
            set([director.name for director in movie.director.all()]),
            set(director)
        )

    def test_from_invalid_omdapi_response(self):
        # When
        serializer = OMDBMovieSerializer(
            data={'Error': 'Not found', 'Response': 'False'}
        )

        # Then
        self.assertFalse(serializer.is_valid())


class TestMovieEndpoint(APITestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse('movie-list')

    @responses.activate
    def test_post_valid_movie(self):
        # Given
        responses.add(responses.GET, 'http://www.omdbapi.com/',
                      json=EXAMPLE_DATA, status=200)

        # When
        response = self.client.post(self.url, {'title': 'Test'})

        # Then
        self.assertEqual(response.status_code, 201)
        data = response.json()
        self.assertEqual(data['title'], EXAMPLE_DATA['Title'])

    @responses.activate
    def test_post_existing_title_redirects(self):
        # Given
        responses.add(responses.GET, 'http://www.omdbapi.com/',
                      json=EXAMPLE_DATA, status=200)
        responses.add(responses.GET, 'http://www.omdbapi.com/',
                      json=EXAMPLE_DATA, status=200)

        # When
        first_response = self.client.post(self.url, {'title': 'Test'})
        response = self.client.post(self.url, {'title': 'Test'})

        # Then
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'],
            reverse('movie-detail', [first_response.json()['id']])
        )

    @responses.activate
    def test_post_movie_not_found(self):
        # Given
        responses.add(responses.GET, 'http://www.omdbapi.com/',
                      json={'Response': "False", "Error": "Not found"},
                      status=200)

        # When
        response = self.client.post(self.url, {'title': 'Test'})

        # Then
        self.assertEqual(response.status_code, 404)
        data = response.json()
        self.assertEqual(data['error'], "Not found")

    def test_post_invalid_request(self):
        # When
        response = self.client.post(self.url, {'invalid': 'Test'})

        # Then
        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertIn('title', data)

    def test_get_movies(self):
        # Given
        movie = MovieFactory()
        movie.save()

        # When
        response = self.client.get(self.url, {'invalid': 'Test'})

        # Then
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['results'][0]['title'], movie.title)
        self.assertEqual(data['results'][0]['id'], movie.id)


class TestCommentsEndpoint(APITestCase):
    def setUp(self):
        super().setUp()
        self.movie = MovieFactory()
        self.movie.save()
        self.other_movie = MovieFactory()
        self.other_movie.save()
        self.url = reverse('comment-list')

    def test_add_comment(self):
        # Given
        self.assertFalse(self.movie.comment_set.exists())

        # When
        response = self.client.post(
            self.url,
            {'movie': self.movie.id, 'text': 'This is a comment'}
        )

        # Then
        self.assertEqual(response.status_code, 201)
        self.assertTrue(self.movie.comment_set.exists())

    def test_get_all_comments(self):
        # Given
        self.movie.comment_set.create(text='text')
        self.other_movie.comment_set.create(text='text2')

        # When
        response = self.client.get(self.url)

        # Then
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], 2)

    def test_get_movie_comments(self):
        # Given
        models.Comment.objects.create(movie=self.movie, text='text')
        models.Comment.objects.create(movie=self.movie, text='text2')
        models.Comment.objects.create(movie=self.other_movie, text='text2')

        # When
        response = self.client.get(self.url, {'movie': self.movie.id})

        # Then
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], 2)
