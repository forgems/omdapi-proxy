from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.TextField()
    year = models.PositiveIntegerField()
    released = models.DateField()
    genre = models.ManyToManyField(Genre)
    runtime = models.DurationField()
    language = models.ManyToManyField(Language)
    country = models.ManyToManyField(Country)
    plot = models.TextField()
    poster = models.URLField()
    rated = models.CharField(max_length=256)
    metascore = models.PositiveIntegerField(null=True)
    imdb_rating = models.FloatField()
    imdb_votes = models.PositiveIntegerField()
    imdb_id = models.CharField(max_length=64, unique=True)
    type = models.CharField(max_length=128)
    box_office = models.FloatField(null=True)
    production = models.TextField()
    website = models.URLField(null=True)
    awards = models.TextField()
    actors = models.ManyToManyField(Person, related_name='played_in')
    writers = models.ManyToManyField(Person, related_name='wrote')
    director = models.ManyToManyField(Person, related_name='directed')

    def __str__(self):
        return self.title


class Comment(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    text = models.TextField()


class Source(models.Model):
    name = models.CharField(unique=True, max_length=256)

    def __str__(self):
        return self.name


class MovieRating(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    value = models.TextField()
