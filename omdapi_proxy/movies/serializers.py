from typing import Dict, Iterable
from urllib.parse import urlencode

from django.db.models import Model
from rest_framework import serializers
from rest_framework.reverse import reverse

from movies import models
from movies.exceptions import MovieExistsException
from movies.fields import (BoxOfficeField, CommaIntegerField,
                           CommaSeparatedField, MetaScoreField,
                           PersonNamesField, RuntimeField, MovieWebsiteField)


# API Serializers
class PersonSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        fields = ['url', 'id', 'name']
        model = models.Person


class RatingSerializer(serializers.ModelSerializer):
    source = serializers.StringRelatedField()

    class Meta:
        model = models.MovieRating
        fields = ['source', 'value']


class MovieSerializer(serializers.HyperlinkedModelSerializer):
    language = serializers.StringRelatedField(many=True, read_only=True)
    genre = serializers.StringRelatedField(many=True, read_only=True)
    country = serializers.StringRelatedField(many=True, read_only=True)
    actors = PersonSerializer(many=True, read_only=True)
    director = PersonSerializer(many=True, read_only=True)
    writers = PersonSerializer(many=True, read_only=True)
    ratings = RatingSerializer(source='movierating_set', many=True,
                               read_only=True)
    comments_url = serializers.SerializerMethodField()

    class Meta:
        fields = ['url', 'id', 'title', 'year', 'released', 'genre', 'runtime',
                  'language', 'country', 'plot', 'poster', 'rated',
                  'metascore', 'imdb_rating', 'imdb_votes', 'imdb_id', 'type',
                  'box_office', 'production', 'website', 'awards', 'actors',
                  'writers', 'director', 'ratings', 'comments_url']

        read_only_fields = [field for field in fields if field != 'title']
        model = models.Movie

    def get_comments_url(self, obj):
        return "{}?{}".format(
            reverse('comment-list', request=self.context.get('request')),
            urlencode({'movie': obj.pk})
        )


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = models.Comment


class RatingSerializer(serializers.Serializer):
    Source = serializers.CharField(source='source')
    Value = serializers.CharField(source='value')


# OMDB API serializer
class OMDBMovieSerializer(serializers.Serializer):
    Title = serializers.CharField(source="title", max_length=256)
    Year = serializers.IntegerField(source="year")
    Rated = serializers.CharField(source="rated", max_length=256)
    Released = serializers.DateField(source="released",
                                     input_formats=["%d %b %Y"])
    Runtime = RuntimeField(source="runtime")
    Genre = CommaSeparatedField(source="genre")
    Actors = PersonNamesField(source="actors")
    Writer = PersonNamesField(source="writers")
    Director = PersonNamesField(source="director")
    Language = CommaSeparatedField(source="language")
    Country = CommaSeparatedField(source="country")
    Plot = serializers.CharField(source="plot")
    Metascore = MetaScoreField(source="metascore", allow_null=True)
    imdbRating = serializers.FloatField(source="imdb_rating", allow_null=True)
    imdbVotes = CommaIntegerField(source="imdb_votes", allow_null=True)
    imdbID = serializers.CharField(source="imdb_id")
    Type = serializers.CharField(source="type")
    Poster = serializers.URLField(source="poster", allow_null=True)
    Production = serializers.CharField(source="production")
    Website = MovieWebsiteField(source="website", allow_null=True)
    BoxOffice = BoxOfficeField(source="box_office", allow_null=True)
    Ratings = RatingSerializer(source='ratings', many=True)

    def get_or_create_related_objects(self, model: Model,
                                      names: Iterable[str], field='name'):
        """
        Create missing Model objects with given names.

        model - a model for which to create instances
        names - iterable of names
        field - unique field name for lookup and creating

        returns a dict of instances with given names
        """
        existing_objects = model.objects.in_bulk(names, field_name=field)
        new_names = set(names) - set(existing_objects.keys())
        new_objects = model.objects.bulk_create([
            model(**{field: name}) for name in new_names
        ])
        existing_objects.update({
            getattr(obj, field): obj for obj in new_objects
        })
        return existing_objects

    def create(self, validated_data: Dict[str, object]):
        data = validated_data.copy()

        genre = data.pop('genre')
        actors = data.pop('actors')
        director = data.pop('director')
        writer = data.pop('writers')
        language = data.pop('language')
        country = data.pop('country')
        ratings = data.pop('ratings')

        instance = models.Movie.objects.filter(imdb_id=data['imdb_id']).first()
        if instance:
            raise MovieExistsException(instance)

        instance = models.Movie.objects.create(**data)
        instance.actors.set(
            self.get_or_create_related_objects(models.Person, actors).values()
        )
        instance.writers.set(
            self.get_or_create_related_objects(models.Person, writer).values()
        )
        instance.director.set(
            self.get_or_create_related_objects(models.Person, director)
            .values()
        )
        instance.language.set(
            self.get_or_create_related_objects(models.Language, language)
            .values()
        )
        instance.genre.set(
            self.get_or_create_related_objects(models.Genre, genre).values()
        )
        instance.country.set(
            self.get_or_create_related_objects(models.Country, country)
            .values()
        )
        sources = self.get_or_create_related_objects(
            models.Source,
            [rating['source'] for rating in ratings]
        )
        models.MovieRating.objects.bulk_create([
            models.MovieRating(
                movie=instance,
                source=sources[rating['source']],
                value=rating['value']
            ) for rating in ratings
        ])

        return instance
