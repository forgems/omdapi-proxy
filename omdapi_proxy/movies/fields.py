import datetime as dt
import re
from rest_framework import serializers, fields


# Helper fields
class RuntimeField(serializers.DurationField):
    """
    Parses runtime field in the form "xxx min".
    """

    regex = re.compile('(?P<value>\d+) min', re.IGNORECASE)

    def to_representation(self, obj):
        return "{:0.0f} min".format(obj.total_seconds() // 60)

    def to_internal_value(self, data):
        return dt.timedelta(
            minutes=int(self.regex.match(data).groupdict()['value'])
        )


class CommaSeparatedField(serializers.CharField):
    """
    Field for comma separated string values.
    """

    def to_representation(self, obj):
        return ", ".join(str(item) for item in obj)

    def to_internal_value(self, data):
        return [val.strip() for val in data.split(',')]


class PersonNamesField(CommaSeparatedField):

    def clean_name(self, name):
        return name.split('(', 1)[0].strip()

    def to_representation(self, obj):
        return map(self.clean_name, super().to_representation())


class NAFieldMixin:
    """
    Mixin class to handle N/A values in some responses returned by OMDBAPI
    """
    def validate_empty_values(self, data):
        if data == 'N/A':
            data = None
        return super().validate_empty_values(data)

    def to_internal_value(self, data):
        if data == 'N/A':
            return fields.empty
        return super().to_internal_value(data)


class MovieWebsiteField(NAFieldMixin, serializers.URLField):
    pass


class MetaScoreField(NAFieldMixin, serializers.IntegerField):
    pass


class CommaIntegerField(NAFieldMixin, serializers.IntegerField):
    """
    Parses a number with commas to distinct thousand values
    """

    def to_internal_value(self, data):
        return super().to_internal_value(data.replace(',', ''))


class BoxOfficeField(NAFieldMixin, serializers.FloatField):
    """
    Parses an amount of money that movie has earned in the form "$389,804,217"
    """

    def to_internal_value(self, data):
        data = data.strip('$')
        return super().to_internal_value(data.replace(',', ''))
