from rest_framework import mixins, response, status, viewsets, reverse

from movies import models, omdb, serializers, exceptions


# Create your views here.
class MovieViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Movie endpoint. Returns a list of stored movies.

    On post, looks up a movie with given title (required) in OMDBAPI and adds
    it to the database. (response code 201 created)
    If the movie already exists, returns 302 response with location set to
    the existing movie url.
    """
    queryset = models.Movie.objects.all()
    serializer_class = serializers.MovieSerializer
    schema = None

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            instance = omdb.fetch_movie_by_title(
                serializer.validated_data['title']
            )
        except omdb.APIError as e:
            return response.Response({'error': str(e)},
                                     status=status.HTTP_404_NOT_FOUND)
        except exceptions.MovieExistsException as e:
            resp = response.Response(
                status=status.HTTP_302_FOUND,
            )
            resp['Location'] = reverse.reverse(
                'movie-detail', args=[e.instance.pk]
            )
            return resp
        serializer = self.get_serializer(instance=instance)
        return response.Response(
            serializer.data,
            status=status.HTTP_201_CREATED
        )


class PersonViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer


class CommentViewSet(mixins.CreateModelMixin, viewsets.ReadOnlyModelViewSet):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    filter_fields = ['movie']
