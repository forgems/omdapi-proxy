from django.contrib import admin
from django.urls import path
from rest_framework.routers import SimpleRouter

from movies.views import (PersonViewSet, CommentViewSet, MovieViewSet)

router = SimpleRouter()
router.register('movies', MovieViewSet)
router.register('person', PersonViewSet)
router.register('comments', CommentViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += router.urls
