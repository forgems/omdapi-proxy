Simple omdbapi-proxy project.
============================

Setup
=====
In order to run this project you first need to obtain omdbapi key from
http://www.omdbapi.com/apikey.aspx

After that you could run the project with simple

```sh
git clone https://gitlab.com/forgems/omdapi-proxy
cd omdapi-proxy
echo "DJANGO_SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxx" > .env
echo "OMDB_API_KEY=yyyyyy" >> .env
docker-compose up
```
Those steps should pull all needed images and build the project.
After that you could simply point the browser at http://localhost:8000/movies
