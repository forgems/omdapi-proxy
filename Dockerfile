from python:3.6.6-stretch
RUN apt update & apt upgrade -y
RUN mkdir /app
ADD requirements.txt /app
WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt
ADD uwsgi.ini /
ADD omdapi_proxy/ /app/
